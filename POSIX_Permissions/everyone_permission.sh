#!/bin/bash -x
# Nick Wood - Jan 2015
# COPYRIGHT - HOGARTHWW
# bash script to apply ACL's on SAN's.
IFS=$'\n\t'

#set variables
#directory to be blatted!
dir=zzz
#dir="VIDEO PROJECTS"
#dir="AUDIO PROJECTS"

cd /Volumes/hgscVideo/
pwd

#Firstly set POSIX’s 
echo “Applying POSIX Permissions”
chmod -Rf 777 $dir
chown root:staff $dir

#Set ACL

echo "Removing existing ACL's"
chmod -RN $dir
echo "Done"

echo "Applying ACL's to top level Folder"
chmod +a "everyone:allow list,add_file,search,add_subdirectory,delete_child,readattr,readextattr,readsecurity,writesecurity,chown" $dir
echo "ACL's applied"

echo “Applying ACL’s to child folders and desceneants”
chmod -R +a "everyone:allow list,add_file,search,delete,add_subdirectory,delete_child,readattr,writeattr,readextattr,writeextattr,readsecurity,file_inherit,directory_inherit,only_inherit" $dir
echo "ACL's applied”

echo "Done with permissions!”


sleep 3