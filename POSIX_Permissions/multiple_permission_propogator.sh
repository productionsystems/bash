# Nick Wood - Jan 2015
# bash script to apply ACL's to multiple folders on SAN's.
IFS=$'\\n\\t'

clear

## define an array ##
arrayname=( "VIDEO PROJECTS" "AUDIO PROJECTS" )

cd /Volumes/hgscVideo/
pwd

## get item count using $\{arrayname[@]\} ##
for m in "$\{arrayname[@]\}"
do
  echo "$\{m\}"
  
		#Firstly set POSIX 
		chown -Rf root:staff $\{m\}
		chmod -Rf 777 $\{m\}

		#Set ACL

		echo "Removing existing ACL's"
		chmod -RN $\{m\}
		echo "Done"

		#echo "Applying ACL's to top level Folder"
		#chmod +a "HOGARTHWW\\Domain Users:allow list,add_file,search,add_subdirectory,delete_child,readattr,readextattr,readsecurity,writesecurity,chown" $\{m\}
		#echo "ACL's applied"

		#echo Applying ACL's to child folders and desceneants
		#chmod -R +a "HOGARTHWW\\Domain Users:allow list,add_file,search,delete,add_subdirectory,delete_child,readattr,writeattr,readextattr,writeextattr,readsecurity,file_inherit,directory_inherit,only_inherit" $\{m\}
		#echo "ACL's applied"

		#applied simplified permisions to HWW domain users
		chmod -R +a "HOGARTHWW\\Domain Users:allow list,add_file,search,delete,add_subdirectory,delete_child,readattr,writeattr,readextattr,writeextattr,readsecurity,file_inherit,directory_inherit" $\{m\}

		echo "Done applying permissions!"

	done

sleep 3

#killall Terminal}
