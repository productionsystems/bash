#!/bin/sh
PATH=/usr/local/bin:/usr/local/sbin:~/bin:/usr/bin:/bin:/usr/sbin:/sbin
mkdir "/var/log/permissionschanges/$(date '+%Y-%b-%d')"
find /Volumes/Video/ProjectMedia -type f -o -type d \( -not -group wheel -o -not -user root -o -not -perm 0777 \) -exec ls -laRd '{}' \; > "/var/log/permissionschanges/$(date '+%Y-%b-%d')/$(date +%Y-%m-%d_%H)_permission_change_Video_POSIX.log" -exec chflags nouchg '{}' \; -exec chown root:wheel '{}' \; -exec chmod 777 '{}' \;
